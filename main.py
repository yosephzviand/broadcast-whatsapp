import pywhatkit as kit
import time
import logging

lyrics=open('peserta.txt')
text = lyrics.read()
lyrics.close()
moblie_no_list = text.split('\n')

image_path = "./wulan.png"
Caption = "Testing WhatsApp Automation"

for moblie_no in moblie_no_list:
        try:
            kit.sendwhats_image(moblie_no, image_path, Caption,15,True)
            print("Successfully Sent to " + moblie_no)
            time.sleep(10)
            logging.basicConfig(filename='success.txt', level=logging.INFO)
            logging.info('Successfully Sent to ' + moblie_no)
        except Exception as e:
            print('Failed to send message to ' + str(moblie_no) + str(e))
            logging.basicConfig(filename='error.txt', level=logging.ERROR)
            logging.error('Failed to send message to ' + str(moblie_no))