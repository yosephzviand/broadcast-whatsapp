Cara menggunakan :

1. Install Aplikasi Python di dalam folder master
2. Install Pywhatkit copy `pip3 install pywhatkit`
3. Masukan gambar yang ingin di kirim ke dalam Folder ini 
4. Sesuaikan nama file di `image_path = "./wulan.png"`
    Ex: `image_path = "./didik.png"`
5. Sesuaikan Pesan di `Caption = "Testing WhatsApp Automation"`
    Ex: `Caption = "Selamat Pagi Salam Sehat"`
6. Masuk ke CMD pastikan berada di dalam folder broadcast
7. Jalankan dengan perintah python3 main.py